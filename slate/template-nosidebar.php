<?php
/**
 * Template Name: No Sidebar
 * Description: Page template like the default page but with no sidebar
 */

$context = Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

Timber::render( array( 'templates/nosidebar-page.twig', 'page.twig' ), $context );

echo get_page_template_slug( $post->ID );