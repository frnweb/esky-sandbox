<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];


$card = new FieldsBuilder('card');

$card
	->addGroup('card', [
			'title' => 'Card'
		])

		//PreHeader		
		->addText('pre_header', [
			'label' => 'Pre-header',
			'wrapper' => ['width' => 70]
		])
			->setInstructions('This is optional')

		// Header
		->addText('header', [
			'label' => 'Header',
			'ui' => $config->ui
		])
		->setInstructions('This is optional')

		// WYSIWYG
		->addWysiwyg('paragraph', [
			'label' => 'WYSIWYG',
			'ui' => $config->ui
		])
		->setInstructions('This is optional')
		
		//Button
		->addFields(get_field_partial('modules.button'))
		  
  ->endGroup();

return $card;