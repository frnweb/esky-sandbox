<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];


$spotlight = new FieldsBuilder('spotlight');

$spotlight
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$spotlight
	->addTab('content', ['placement' => 'left'])
		//Header
		->addTrueFalse('add_header', [
			'label' => 'Add Header',
		])
		->setInstructions('Optional pre-header for the card')
		
		->addText('header', [
			'label' => 'Header',
		])
		->conditional('add_header', '==', 1 )

	  	// WYSIWYG
	  	->addTrueFalse('add_wysiwyg', [
			'label' => 'Add Wysiwyg',
		])
		->setInstructions('Optional wysiwyg for the card')
		->addWysiwyg('wysiwyg', [
			'label' => 'Wysiwyg',
		])
		->conditional('add_wysiwyg', '==', 1 )

		//Button
		->addFields(get_field_partial('modules.button'));
	

return $spotlight;

