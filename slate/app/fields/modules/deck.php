<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$deck = new FieldsBuilder('deck');

$deck
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'))
		->addFields(get_field_partial('partials.grid_options'));

$deck
	->addTab('content', ['placement' => 'left'])

		//Header
		->addText('header', [
			'label' => 'Deck Header'
	    	])
	    	->setInstructions('This is optional')

		//Repeater
		->addRepeater('deck', [
		  'min' => 1,
		  'max' => 10,
		  'button_label' => 'Add Card',
		  'layout' => 'block',
		])
		->addTrueFalse('link_wrapper', [
			'wrapper' => ['width' => 15]
			])
			->setInstructions('Check to make whole card a clickable link')
		->addLink('link_url', [
			'wrapper' => ['width' => 85]
		])
		->conditional('link_wrapper', '==', 1)

		//Image 
		->addImage('deck_image')
			->setInstructions('This is optional')
		// Card
	  	->addFields(get_field_partial('modules.card'));
    
return $deck;