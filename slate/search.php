<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$templates = array('search.twig','archive.twig', 'index.twig' );

$context = Timber::get_context();
$context['title'] = get_search_query();
$context['posts'] = new Timber\PostQuery();
$context['pagination'] = Timber::get_pagination();

$search = get_search_query();

$new_args = array(
    'post_type'      => 'post',
    'posts_per_page' => '4', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date',
);

$context['get_newest'] = new Timber\PostQuery( $new_args );

Timber::render( $templates, $context );
