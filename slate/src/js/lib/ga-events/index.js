(function (i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-3020285-2', 'auto');
    ga('send', 'pageview');
  
  //GA EVENTS
  $(document).ready(function() {
  
    var label = window.location.pathname;
  
    //GA Event for Slick Carousel prev button
    $('.slick-prev').click(function() {
      ga('send', 'event', 'carousel', 'previous', 'carousel on' + label);
    });
  
    //GA Event for Slick Carousel next button
    $('.slick-next').click(function() {
      ga('send', 'event', 'carousel', 'next', 'carousel on' + label);
    });
  
    //GA Event for open Modal button
    $('.modal-button').click(function() {
      ga('send', 'event', 'modal', 'open', 'modal open on' + label);
    });
  
    //GA Event for close Modal button
    $('.close-button').click(function() {
    ga('send', 'event', 'modal', 'close', 'modal close on' + label);
    });
  
    //GA Event for Menu search button
    $('.sl_button--search').click(function() {
      ga('send', 'event', 'menu search', 'click', 'Menu Search on ' + label);
    });
  
     //GA Event for Mobile Menu open button
    $('.is-active, .hamburger--collapse').click(function() {
      ga('send', 'event', 'mobile menu toggled', 'click', 'Mobile menu toggled on' + label);
    });
  
  });